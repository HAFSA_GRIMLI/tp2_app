package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<Book> book;

    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        book = repository.getSelectedBook();
    }

    public MutableLiveData<Book> getBook() {
        return book;
    }

    public void setBook(long id) {
        if (id == -1) {
            book = null;
        }
        else {
            repository.getBook(id);
            book = repository.getSelectedBook();
        }

    }

    public void insertOrUpdateBook(String title, String authors, String year, String genres, String publisher) {
        if (book != null) {
            Book updatedBook = book.getValue();
            updatedBook.setTitle(title);
            updatedBook.setAuthors(authors);
            updatedBook.setYear(year);
            updatedBook.setGenres(genres);
            updatedBook.setPublisher(publisher);
            book.setValue(updatedBook);
            repository.updateBook(updatedBook);
        }
        else {
            Book newBook = new Book(title, authors, year, genres, publisher);
            book = new MutableLiveData<>(newBook);
            repository.insertBook(newBook);
        }
    }
}
